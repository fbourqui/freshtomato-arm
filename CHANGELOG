FreshTomato-ARM Changelog
===========================

2019.2 - 2019.04.20
----------------------------

- openssl: update to 1.0.2r
- SQLite: update to 3.27.2
- php: update to 7.2.17
- dnsmasq: update to 2.80-343b7b4 snapshot
- libcurl: update to 7.64.1
- nano: update to 4.0
- dropbear: update to 2019.78
- pppd: clean sources 2.4.5, add patches instead
- pppd: update to 2.4.6
- pppd: update to 2.4.7
- pppd: fixes from upstream
- miniupnpd: update to 2.1.20190408
- libyaml: update to 0.2.2
- getdns: update to 1.5.2 + upstream build error fix
- getdns: add patch to fix missing define for log_warn
- libubox: update to eeef7b5 snapshot
- patches: miniupnpd: fix naming, cosmetics
- patches: libcurl: cosmetics
- ebtables: add 2 patches (Check -C parameters correctly, Check port range correctly)
- OpenVPN: change the default order of Negotiable Ciphers
- OpenVPN: fix generating an openvpn client configuration on server with TLS authorization (enable remote-cert-tls)
- router: Makefile: build openssl with no SSLv2 and SSLv3 support
- router: Makefile: clean-up openvpn recipe
- router: Makefile: clean-up miniupnpd build recipe
- router: Makefile: build dnsmasq with HAVE_AUTH flag
- router: Makefile: fix emf install
- router: rc: misc.c: clean-up
- router: rc: usb.c: add support for Router with two USB LEDs / Ports (according to LED table at shared/led.c)
- router: rc: usb.c: change R8000 assignment for USB2/USB3 (and some cosmetic)
- router: rc: usb.c: some cosmetic at function usbled_proc(...) / align to sdk7
- router: rc: usb.c: change R7000 assignment for USB2/USB3 (only cosmetic)
- router: rc: services.c/network.c/usb.c: clean-up and cosmetics
- router: rc: services.c: add function disable_led_wanlan() to have more compact code
- router: rc: services.c: add stealth mode also for R8000 and AC3200
- router: rc: network.c - change blink behaviour / start
- router: rc: services.c: add some logging when starting/stopping services
- router: rc: services.c: change the way how "serial" and "uuid" are created in minidlna config
- router: rc: services.c: cosmetics
- router: rc: rc.h - add missing prototype declaration for function start_phy_tempsense() and stop_phy_tempsense()
- router: shared: misc.c: correct insufficient number of snprintf arguments
- router: rc: change the name of the ntpc service to ntpd + some code changes, in accordance with other start/stop functions
- router: rc: blink_br.c - small fix for the Router RT-AC56U - distinguish two cases right now: LAN Port 0-1-2-3 or 1-2-3-4
- router: rc: init.c: change back to where the start_wan() function is called
- router: rc: init.c: Reverted "Change back to where the start_wan () function is called"
- router: rc: init.c: change min_free_kbytes setting - catch up to AsusWRT / Merlin and also Netgear (case 20 MByte right now, was 14 MByte)
- router: rc: init.c: tune SMP
- router: rc: vpn.c: fix client/server start on NOSMP routers
- router: rc: vpn.c: increase interface queue length from 100 to 1000 bytes
- router: rc: clean-up (cosmetics)
- router: rc: services.c and shared: led.c: do some cleanup and cosmetic - move all functions for LEDs into led.c - rename start_led_setup() to led_setup - add function enable_led_wanlan()
- router: others: wwansignal: fix showing the 3G signal level
- router: others: switch3g/switch4g/wwansignal: some improvements/fixes
- router: www: tools-shell.asp: support of multiple lines pasted into termlib window
- router: www: status-overview.asp: a few W3C fixes
- router: www: Makefile: remove more obsolete stuff from html
- router: www: qos-graphs.asp: fix W3C again
- router: www: another W3C fixes
- router: httpd/rc: vpn.c: replace &buffer[0] (and &buffer2[0], &buf[0]) references in openvpn with straight buffer, for better readability and reduced risk of errors
- router: httpd: correct generation of HTTPS certificate
- router: httpd: wwan.c: fix compiler warning
- router: nvram: defaults.c: cosmetics - rebranding ;)
- router: pdureader: fix compiler warning
- router: utils: robocfg.c - catch up to AsusWRT / Merlin (thx) - one file for both, ARM and MIPS
- GUI: Reverted "Wireless Settings: remove obsolete settings"
- GUI: Wireless Settings: remove obsolete settings (antennas)
- GUI: PPTP Client: increase max length of "server address" to 50 chararacters
- GUI: QOS: fix JS error on View Details page, when view in given class
- GUI: QOS: fix (again) some problems on View Details page
- GUI: QOS: fix table sorting by "Protocol" on View Details page
- GUI: OpenVPN: remove support for the RC ciphers. DES is kept for now, for legacy reasons
- GUI: OpenVPN: Fix vpn-server.asp visible key fields
- GUI: fix at last Wireless Ethernet Bridge mode - just refresh (Ctrl + F5) Basic -> Network page, and click "Save"
- GUI: fix ports order caused by commit #7cb2220 + clean-up
- GUI: add support of WWAN modem signal - use a minimum of 10 seconds of refresh time for best readings on Status -> Overview page
- GUI: add support of multi WAN in modem status
- GUI: add support of SMS inbox for 4G non-hilink/3G modems
- GUI: advanced-routing.asp - add option to force IGMPv2 - cosmetic
- R6250: change LED table + LED table cleanup
- R6300v2: change LED table + LED table cleanup
- R6400: change LED table + LED table cleanup
- R7000: change LED table + LED table cleanup
- R8000: change LED table + LED table cleanup
- WS880: change LED table + LED table cleanup
- RT-N18U: change LED table + LED table cleanup
- RT-AC56U: change LED table + LED table cleanup
- RT-AC68U: change LED table + LED table cleanup
- EA6400: change LED table + LED table cleanup
- EA6700: change LED table + LED table cleanup
- EA6900: change LED table + LED table cleanup
- DIR868L: change LED table + LED table cleanup
- LEDs: stealth mode (part 1) - extend already existing stealth mode and turn off GPIO LEDs - do not start blink / blink_br with stealth mode turned on
- LEDs: stealth mode (part 2) - extend already existing stealth mode and turn off WAN & LAN Port LEDs at the ethernet connectors or front panel/case - reboot is requiered right now after enabling stealth mode! - code/stealth mode will be extended...
- R6250/R6300v2/AC15U/DIR868L: add to stealth mode
- Add support for D-Link DIR868L rev C
- WL: update wireless driver for SDK7 to GPL 382.51374
- Raise revision level to allow initial files install from stock NETGEAR
- Raise revision level to allow initial files install from stock NETGEAR (for SDK7)


2019.1 - 2019.02.27
----------------------------

- OpenVPN: update to 2.4.7
- tor: updated to 0.3.5.8
- dnsmasq: update to 2.80-28cfe36 snapshot (add back ability to compile without IPv6 support to minimize size of dnsmasq (as a patch), cosmetics in other patches, little cleanup in router/Makefile)
- miniupnpd: update to 2.1.20190210
- patches: cosmetics in miniupnpd
- SQLite: update to 3.27.1
- php: update to 7.2.15
- libcurl: update to 7.64.0
- libcurl: Updated CA certificate bundle as of 2019-01-23
- nettle: update to 3.4.1
- adminer: update from 4.7.0 to 4.7.1 2019-01-24
- getdns: update to 1.5.1 (stubby 0.2.5)
- stubby: change round_robin_upstreams to 1
- stubby: add Google DNSoTLS (ipv4/ipv6) to stubby.yml
- tinc: revert: Use git describe to populate autoconf's VERSION
- kernel: drivers: net: usb: rndis_host.c: fix init of the module
- kernel: drivers: net: usb: rndis_host: Set valid random MAC on buggy devices
- kernel: drivers: net: usb: rndis_host: support Novatel Verizon USB730L
- kernel: etherdevice: Use ether_addr_copy to copy an Ethernet address
- kernel: net: netfilter: nf_conntrack_proto_tcp.c: reduce TCP_CONNTRACK_ESTABLISHED default value to 20 minutes
- DDNS: opendns requires now HTTP 1.1 in request header
- router: httpd: fix warnings in compiler; clean-up
- router: httpd: tomato.c: additional commit for #0660a82 and #c1d1c76
- router: httpd: misc.c: cosmetics
- router: mdu: mdu.c: clean-up & simplify some if conditions
- router: mdu: fix compiler warnings
- router: mdu: remove no more needed functions/files/includes
- router: rc: fix warnings in compiler; clean-up
- router: rc: init.c: enable blink on R8000
- router: rc: network.c: include 2nd 5Ghz radio for Wifi LED status for Netgear R8000
- router: rc: network.c: tweak blink startup code for 2nd 5Ghz LED
- router: rc: network.c: turn on/off WiFi status LED according to the overall radio statuses
- router: rc: network.c: tweak conditional for blink startup on 5Ghz radio
- router: rc: network.c: clean-up
- router: rc: network.c: cosmetics
- router: rc: dhcp.c: remove unused variable
- router: rc: dhcp.c: clean-up
- router: rc: ppp.c: add missing TRACE_PT("end\n")
- router: rc: wan.c: cleanup WAN LED control
- router: rc: blink_5g.c: old blink 5g code is obsolete
- router: rc: blink_5g.c: fix popen/pclose
- router: rc: pbr.c: cosmetics
- router: rc: vpn.c: make instance run code handle more than 1 CPU core
- router: rom: etc: remove unneeded .gitignore file
- router: Makefile: add build progress indicator
- router: Makefile: mv huawei_ether to extras if needed, remove unneeded call to patch for nano
- router: Makefile: clean-up, remove unused ntpclient and ntpc
- router: Makefile: add missing pcre make
- router: Makefile: build mysql --without-docs
- router: Makefile: remove unused libsub
- router: Makefile: move udpxy build
- router: Makefile: add pptpd make part
- router: Makefile: add missing make notices
- router: Makefile: move kernel modules to proper directory
- router: Makefile: pptp-client: remove (forgotten) obsolete sh ip scripts
- router: Makefile: don't ln /usr/share to /tmp when samba3 is installed
- router: Makefile: build nginx with http v2 module
- router: Makefile: fix tor build failures
- router: shared: fix warnings in compiler
- router: shared: fix warnings in compiler; clean-up
- router: shared: misc.c: correction of the "if" condition
- router: shared: misc.c: add/fix missing fclose(...) and some cosmetic
- router: shared: misc.c: change/fix function wan_led(int mode) --> call by value
- router: shared: misc.c: fix a few typos (wrong type, pointer by mistake) at function wan_led_off(...) and check_wanup(...)
- router: shared: misc.c: add 2nd 5Ghz LED for R8000
- router: shared: misc.c: tweak behavior of WLAN/5G LEDs
- router: shared: misc.c: cosmetic / optimization
- router: shared: misc.c: fix popen/pclose
- router: shared: led.c: increase value related to gpio indexing limit scheme to cater for Netgears R8000
- router: shared: led.c: extend GPIO pin support from 0-15 to 0-31
- router: shared: led.c: clean-up; remove unused code
- router: shared: shared.h: add missing prototype declaration for function wan_led(...) and wan_led_off(...)
- router: www: basic-ddns.asp: clean-up of vars, remove unneeded js code
- router: www: vpn-client.asp: change allowed server address length to 60 characters (it's amazing that such long addresses exist ...)
- router: www: about.asp: Cosmetics
- vpnrouting: fix cleaning of routing after stopping the OpenVPN client with "Redirect through VPN" checked - not working from the very beginning (commit that adds this function, also responsible for the error: https://bitbucket.org/pedro311/freshtomato-mips/commits/4c75d36f6fb2c1da1de8d7db33e2a91714d045e8)
- switch4g: fix path for DIAG device in qmi_wwan mode
- switch4g: add support for rndis protocol
- DDNS: FreeDNS: add possibility to update IP with custom value as on other services, add https
- DDNS: HE.net IPv6 Tunnel Broker uses now Dyn DNS Update API http://dyn.com/support/developers/api/
- GUI: Wireless Settings: remove obsolete settings
- GUI: Wireless Filter: add a warning about the number of MAC addresses supported + cosmetics
- GUI: fix IPv6 mask matches
- GUI: fix generate vpn client config
- GUI: QOS: hide View Details when QOS is disabled
- GUI: OpenVPN: increase max length of client common name to 255 chararacters
- GUI: add option for OpenVPN server to force IPv4 or IPv6 for connection
- GUI: add option for OpenVPN client to choose IPv4 or IPv6 only connection
- GUI: OpenVPN ServerX & ClientX - restrict option/setting "Poll Interval" to 0-30 minutes (values > 30 are not usefull)
- Add support for Asus RT-AC3200 with 128k NVRAM - new targets: ac3200-128e and ac3200-128z, use CFE, Asus Firmware Restoration, tftp, DD-WRT fw page update, to upload the firmware to the router on Asus OFW (with already changed NVRAM size to 128k)
- R8000: change LED table
- R6400: change LED table
- R7000: change LED table
- R7000 / R6400: WLAN LED cleanup
- EA6700: enable/activate WAN LED
- WS880: WLAN LED cleanup - use blink for WLAN LED (same like for Netgear R7000)
- Improved a little bit build progress indicator


2019.1.015-beta - 2019.01.10
----------------------------

- kernel: ipv6: use ND_REACHABLE_TIME and ND_RETRANS_TIMER instead of magic number
- kernel: ipv6: drop packets when source address is multicast
- kernel: ipv6: don't accept multicast traffic with scope 0
- kernel: ipv6: don't accept node local multicast traffic from the wire
- kernel: ipv6: drop non loopback packets claiming to originate from ::1
- kernel: ipv6: ip6_forward: perform skb->pkt_type check at the beginning
- kernel: ipv6: drop frames with attached skb->sk in forwarding
- kernel: ipv4: ip_forward: perform skb->pkt_type check at the beginning
- kernel: ipv4: ip_forward: Drop frames with attached skb->sk
- kernel: net: ipv4: igmp.c: bonding: fix to rejoin multicast groups immediately
- kernel: net: ipv4: igmp.c: igmp: Reduce Unsolicited report interval to 1s when using IGMPv3
- kernel: net: ipv4: igmp.c: Make igmp group member RFC 3376 compliant
- udpxy: update to 1.0.23-12, clean sources
- udpxy: fix start with PPP connection
- udpxy: extend GUI function (advanced-firewall.asp)
- e2fsprogs: update to 1.44.5
- miniupnpd: update to git snapshot from 20181218
- miniupnpd: do not disable port forwarding when in double NAT / CGNAT
- GUI: adblock: update lists immediately, if called from the GUI
- GUI: MultiWAN Routing: increase the maximum number of digits to 80 for Port
- GUI: tinc: fix errors caused by commits #eadba155 and #9a391ecc
- GUI: basic-ipv6.asp - only small cosmetic changes/corrections
- router: shared: misc.c: make function check_wanup_time() mwan-ready; small change/adjustment to rstats & cstats to use the new function; cosmetic for rstats & cstats at function calc(): add typecast (long) to meet variable wanuptime (long)
- router: httpd: misc.c: use function check_wanup_time(char *prefix) for void asp_link_uptime(int argc, char **argv) to get the link uptime (wanX)
- router: rc: vpn.c: cosmetics - as close as possible to MIPS version
- router: rc: wan.c: cosmetics - stay as close as possible to MIPS version
- router: rc: wnas.c: cosmetics - stay as close as possible to MIPS
- router: rc: tomatoanon.c: cosmetics - stay as close as possible to MIPS version
- router: rc: tinc.c: cosmetics
- dnsmasq: fix router reboots, when connected to wifi with specific configuration (it's (theoretically) only needed in MIPS branch, but who knows)


2018.5 - 2018.12.21
----------------------------

- openssl: updated to 1.0.2q
- openssl: make proper call to openssl Configure script
- gmp: Move .gitignore to proper directory
- adminer: Updated to 4.7.0
- SQLite: Updated to 3.26.0
- xl2tpd: Updated to 1.3.13
- php: updated to 7.2.13
- nginx: updated to 1.14.2
- rp-pppoe: updated to 3.13
- miniupnpd: updated to git snapshot from 20181206
- libcurl: updated to 7.63.0
- libcurl: updated CA certificate bundle as of 2018-12-05
- comgt: clean sources of v 0.32, add patches instead
- GUI: new termlib based tools-shell.asp
- GUI: simple workaround for supporting cd in tools-shell
- router: httpd: tomato.c: fix the correct length of wanX_modem_dev variables
- router: httpd: vpn.c: use system() instead run_program()
- router: www: admin-access.asp: change allowed password length to 60 characters
- router: www: vpn-client.asp: change allowed server address length to 40 characters
- router: www: about.asp: Cosmetics
- router: www: fixes for W3C + some cosmetics
- router: www: qos-classify.asp: change allowed port length to 130 characters
- router: rom: Makefile: decrease number of tries to 1 for wget. It's already in loop
- router: rc: firewall.c: allow responses from the dhcpv6 server (Port 547) to the client (Port 546) (--> add Server Port 547)
- switch4g: fix variable initialization in modemReset() function
- switch3g/switch4g: add info to log about successful PIN verification
- IPv6: extend GUI status page (status-overview.asp) - show IPv6 addresses for interface wan, br0, br1, br2 and br3
- IPv6: DHCPv6 PD: small corrections - fix visibility for "Request /64 subnet for" --> right now only applicable for DHCPv6 with PD (and not for Native/Static IPv6) - cosmetic for ipv6_pdonly visibility
- IPv6: DHCPv6 PD: - override the default EUI-64 address selection and create a very userfriendly address for br0...br3 (--> ends with ::1 now) - cosmetic - add some comments
- IPv6: small change for DNSMASQ DHCPv6 start address (new ::2 up to ::FFFF:FFFF); leave ::1 address for the router interface brX --> used with DHCPv6-PD (WIDE-DHCPv6) now
- WIDE-DHCPv6: Fix manpages This patch fixes wide-dhcpv6 manpages (paths, typos, ...)
- WIDE-DHCPv6: Don't strip binaries This patch prevents wide-dhcpv6 build system from stripping built binaries
- WIDE-DHCPv6: Make sla-len config optional
- WIDE-DHCPv6: Make sla-id config optional
- WIDE-DHCPv6: cflag patch
- WIDE-DHCPv6: Fix parallel make race condition
- WIDE-DHCPv6: Adding ifid option to the dhcp6c.conf prefix-interface statement
- kernel: netfilter: ip6_tables: fix information leak to userspace
- kernel: ipv6: Warn users if maximum number of routes is reached
- kernel: ipv6: fix overlap check for fragments
- kernel: netfilter: ipv6: fix overlap check for fragments
- kernel: bridge: Fix IPv6 multicast snooping by storing correct protocol type
- kernel: bridge: Fix IPv6 multicast snooping by correcting offset in MLDv2
- kernel: bridge: Add missing ntohs()s for MLDv2 report parsing
- kernel: inet6: prevent network storms caused by linux IPv6 routers
- kernel: ipv6: udp: fix the wrong headroom check
- kernel: bridge: mcast snooping, fix length check of snooped MLDv1/2
- kernel: ipv4: correct IGMP behavior on v3 query during v2-compatibility mode
- Revert "leds and stealth mode rework", it should be checked and tested with given router models first
- Updated README.md, "HOW TO COMPILE"


2018.5.083-beta - 2018.11.25
----------------------------

- kernel: drivers: net: usb: qmi_wwan.c: fix CVE-2017-16650
- kernel: net: ipv4: fix multipath RTM_GETROUTE behavior when iif is given
- kernel: net: ipv6: accept RA and send RS while configured as router
- kernel: proc/sysctl: fix the int overflow for jiffies conversion
- OpenVPN: add TLS keys generator in GUI for VPN Server. Add ability to generate VPN client configuration for TLS
- dnsmasq: change default dns priority to 'no-resolv'
- dnsmasq: improve insecure ds syslog to handle servers that really do not support dnssec
- dnsmasq: update to 2.80
- stubby: make tls_authentication REQUIRED
- nf_conntrack_rtsp and nf_nat_rtsp: update to version 0.7, correcting nat_rtsp's behavour so it now will strip destination addresses that are not a stunaddr and replace with the masquerade IP of the host.
- tor: Updated to 0.3.4.9
- tor: make tor fully functional, so users can solve xxx.onion website dns and visit tor sites
- nano: Updated to 3.2
- php: updated to 7.2.12
- miniupnpd: update to git snapshot from 20181031 (includes PCP fix)
- tinc: Updated to 1.1pre17
- SQLite: Updated to 3.25.3
- nginx: updated to 1.14.1
- snmpd: Updated to 5.8
- uqmi: update to uqmi-01944dd
- apcupsd: update to 3.14.14
- libubox: update to libubox-c83a84a, clean sources, add patch instead
- libcurl: Updated to 7.62.0
- libcurl: Updated CA certificate bundle as of 2018-10-17
- dropbear: fix from upstream for CVE-2018-15599
- Remove residues in the code after ARIA2
- nano: bindings: when Ctrl+Shift+Delete has no keycode, don't use KEY_BSP
- mssl: Updated cipher list
- mssl: fix ssl context ciphers & options wasn't applied
- mssl: fix CVE-2009-3555, various security improvements
- mdu/mssl: add TLS SNI support
- mdu: fix warnings in compiler + cosmetics
- adblock: clean-up, fixes, improvements
- adblock: decrease timeout for wget to a reasonable value
- adblock: fix race condition when wan is up
- IGMP: Resolve CVE-2012-0207 - Resolve potential for divide by 0, allowing remote attackers to cause a denial of service via IGMP packets
- router: preparation of variables for new version of switch4g/switch3g
- router: preparation of variables for new version of switch4g/switch3g part 2
- router: Makefile: Fix nano not working on dir868l target due to missing library
- router: Makefile: add libnfnetlink-clean target
- router: Makefile: add stubby to targets o (R1D) and dir868l
- router: Makefile: Filter support for PHP needs to be enabled for h5ai
- router: Makefile: fix typo
- btools: libfoo.pl: fix typo
- router: www: qos-graphs.asp: Hide "Zoom Graphs" because it doesn't work anyway
- router: www: status-devices.asp: fix the freeze in Vivaldi browser 
- router: www: vpn-tinc.asp: small js fix
- router: www: basic-ipv6.asp AND rc: dhcp.c - some cosmetic - add missing verifcation for lanX_ipv6 - add/change comments (also at file httpd/tomato.c) - add additional check before we request a prefix for br1/br2/br3
- router: www: tomato.js: small fixes
- router: www: red.css: cosmetics
- router: www: Makefile: small fix regarding remove of obsolete stuff from html
- router: www: Makefile: cleanup comments more aggressively
- router: www: Makefile: cosmetics
- router: httpd: increase HTTP_MAX_LISTENERS to 16
- router: httpd: wl.c: fix popen/pclose
- router: httpd: vpn.c: cosmetics
- router: httpd: tomato.c: sync NVRAM variables sequence of OpenVPN Server 1 and Server 2 - add missing default-values for variables "vpn_server1_userpass" and "vpn_server1_nocert"
- router: httpd: tomato.c: cosmetics
- router: httpd: iperf.c: change the location of the pid file
- router: rc: vpn.c: add some comments -protection/cosmetic within function start_vpn_eas() and stop_vpn_eas: add check that i (counter for Server X/Client Y) will always be < 4 before write value to nums[i]
- router: rc: led.c: fix led applet - use proper led in case usb3
- router: rc: init.c: cosmetics
- router: rc: services.c: fix: Static DNS settings broken with WAN disabled (i.e. operating as AP)
- router: rc: network.c: Do not enable IPv6 for 'all', 'eth0', 'eth1', 'eth2' (ethX) - IPv6 will live on the bridged instances
- router: dhcp.c: add some comments -cosmetic -change *lanif to const char (pointer can be changed but not char), because of getifaddr return value (const char*) -remove semicolon after some if-conditions
- router: shared: led.c: also for USB GPIO values in case AC56/68U
- router: shared: defaults.c: change default value for ntp_updates (Auto Update Time) to 1 (Auto interval)
- router: shared: defaults.c: small fix for vpn ca key
- router: config_base: add missing TCONFIG_IPERF
- router: mssl: mssl.c: fix build break on dir868l target
- small fix for IPv6 accept_ra: make it possible to change accept_ra value for WAN and LAN(br0...br3) without reboot/restart of the router
- IPv6: restrict Accept RA from LAN option (with dnsmasq)
- IPv6: small fix/changes for DHCPv6 with Prefix Delegation - let IPv6 RA via WAN take care of adding the default route
- switch3g: change sleep time for switching modem
- switch3g: rework (1/2)
- switch4g: change the sleep time for 2nd type non-hilink modem, before send any command (some devices need this, otherwise they hang)
- switch4g: add more possible options to Network Type and Roaming for 2nd type (qmi-wwan) non-hilink modems
- switch4g: rework
- switch4g: do not search all DIAGS every time - use already found for given device in searchDiag()
- vpnrouting: cosmetics
- GUI: adblock: add warning
- GUI: stubby: add the ability to choose the level of logging
- GUI: add CPU / WL temperature readings in Fahrenheit degrees
- GUI: add feature to generate VPN static key from GUI
- GUI: add warning on OpenVPN server page about needed free NVRAM space
- GUI: add option for OpenVPN LZ4-V2 compression
- GUI: Add IPERF bandwidth test tool with as an option
- GUI: Generation of iperf commandline
- GUI: IPerf: fix some minor JS bugs
- Fix build when valgrind is installed on host
- www: W3C never-ending-story
- Final clean-up of UI files according to the Web Consortium W3C standard
- Fix PHP build when libicu is installed
- Fix build when LZMA is installed on host
- Fix "cannot run test program while cross compiling"
- patches: fix mysql re-check patch
- cosmetic and small updates for IPv6
- Stealth Mode switch for LEDs
- LEDs and stealth mode rework


2018.4 - 2018.09.12
----------------------------

- Preliminary support for Stubby (DNS-over-TLS)
- dnsmasq: Updated to 2.80test6
- openssl: updated to 1.0.2p
- php: Updated to 7.2.9
- tor: Updated to 0.3.3.9
- tinc: Updated to 1.1pre16
- libcurl: Updated to 7.61.1
- libcurl: Fix build failures
- e2fsprogs: Updated to 1.44.4
- libcurl: Updated CA certificate bundle as of 2018-06-20
- adminer: Updated to 4.6.3
- miniupnpd: Updated to 2.1.20180706
- libjson-c: Updated to 0.13.1
- samba: enable PARALLEL_BUILD directive for components
- gmp: optimize gmp build (fix compilation with different autotools version, allow parallel make, don't build demos and doc)
- mdadm: skip building mdadm man pages
- igmpproxy: fix compiler flags, change code optimization to -O3
- dnscrypt-proxy: Updated resolvers csv to 20180709
- Increase the maximum size that is used when reading the ssh-host-key (to 4096 bits)
- OpenVPN: make IPv6 connection possible if IPv6 is enabled
- OpenVPN: extend Server GUI functionality - add option to push LAN(br0)...LAN4(br3) (only if available) - push the suitable DNS Server LAN IP
- radvd: remove leftovers at file router/rc/rc.h (Tomato uses dnsmasq)
- GUI: only include curl as a connection checker, if it's built
- GUI: openvpn: add AES-*-GCM ciphers to the available legacy ciphers
- GUI: add a needed include file for code utilizing bwm-common.js
- GUI: bwm-common.js: fix erroneous change in commit 3e650c1
- GUI: wireless.js: fix erroneous change in commit fe53904
- GUI: do not display rt bw graphs if monitoring has been disabled
- router: Makefile: compile dnsmasq with NO_ID, NO_AUTH and NO_GMP directive + some cosmetics
- router/rc/wan.c: start miniupnpd after httpd/later to avoid disabling IPv6 at miniupnpd startup (does happen sometimes with 2018.3, solves miniupnpd warning "no HTTP IPv6 address, disabling IPv6" at reboot/restart)
- router/rc/transmission.c: sysctl binary is not included in TomatoUSB, write values directly instead
- router/rc/rc.h: fix ARM builds WITHOUT IPv6 support (there is no freshtomato ARM build with IPv4 support only)
- router/rc/firewall.c and rc.h - add function "enable_ndp_proxy()" - Enable NDP Proxy for IPv6 builds - add missing conditional compilation
- watchdog: increase waittime to 3 and max_ttl to 4 in traceroute to reduce false positives
- nocat: Retiring Captive Portal feature
- kernel: netfilter: fix u32 match
- kernel: netfilter: nf_conntrack: fix count leak in error path of __nf_conntrack_alloc
- kernel: netfilter: nf_conntrack: set conntrack templates again if we return NF_REPEAT
- kernel: netfilter: nf_conntrack: fix early_drop with reliable event delivery
- kernel: netfilter: nf_conntrack: fix ct refcount leak in l4proto->error() (Tomato doesn't have icmp module, but this fix is still relevant)
- kernel: netfilter: nf_conntrack: fix event flooding in GRE protocol tracker
- kernel: netfilter: ip6_route_output() never returns NULL. ip6_route_output() never returns NULL, so it is wrong to check if the return value is NULL
- kernel: netfilter: ip4 ip_queue: Fix small leak in ipq_build_packet_message()
- kernel: netfilter: ip6 ip_queue: Fix small leak in ipq_build_packet_message()
- kernel: netfilter: ipset: dumping error triggered removing references twice
- kernel: netfilter: ebtables: fix wrong name length while copying to user-space
- kernel: logfs: Prevent memory corruption
- kernel: cifs: fix possible memory corruption in CIFSFindNext
- kernel: ARM: 6891/1: prevent heap corruption in OABI semtimedop
- kernel: ext3: Fix error handling on inode bitmap corruption
- kernel: ext2: Fix error handling on inode bitmap corruption
- kernel: mac80211: fix conn_mon_timer running after disassociate
- patches: dnsmasq: log packet resize reports at debug level instead of warning since they are too frequent
- WL: update wireless driver for SDK7 to GPL 382.50470
- Fixing the `uname -r` issue in readme


2018.3 - 2018.06.22
----------------------------

- php: updated to 7.2.7
- dnsmasq: update to 2.80test2
- iptables: updated to to 1.6.2
- libcurl: updated to 7.60.0
- nano: updated to 2.9.8
- sqlite: updated to 3.24.0
- tor: Updated to 0.3.3.7
- xl2tpd: Updated to 1.3.12
- entware: download installer scripts over https
- dnscrypt-proxy: remove unneeded public-resolvers.md file from build
- dnscrypt-proxy: define own timeout and number of tries for wget to use local copy of server list much quicker than with defaults
- www: tools-wol.asp: WOL bugfix
- www/status-overview.asp: fix wireless show/hide state retension
- www: advanced-vlan.asp: cosmetics
- www: status-overview.asp: cosmetics
- router/www: advanced-tor.asp: fix search for specified words
- router/www: advanced-tor.asp: allow to enter "SocksPort" also in Custom Configuration
- router/Makefile: add PARALLEL_BUILD directive to dhcpv6
- router: httpd/rc: fix warnings in compiler
- router: rc: fix warnings in compiler
- kernel: tweak input class modules, removing mouse/joystick support


2018.3.018-beta - 2018.05.27
----------------------------

- OpenVPN: updated to 2.4.6
- php: updated to 7.2.6
- miniupnpd: updated to 2.1
- dnsmasq: updated to 2.80test2
- ipset: updated to 6.38
- nginx: updated to 1.14.0
- nano: updated to 2.9.7
- transmission: updated to 2.94
- snmpd: updated to 5.8.rc2
- e2fsprogs: updated to 1.44.2
- tor: updated to 0.3.3.6
- EBTABLES: updated to master-head as at May 25, 2018
- BRIDGE-UTILS: updated to 1.6 (plus commits in master as at May 7, 2018)
- ntpclient: updated to 2017_246
- Switch from ntpc to ntpclient - Added code to handle previous issues (not update on reboot, etc)
- Transition from using ntpclient (or ntpc) to Busybox ntpd
- Clean ups in ntp start proc
- igmpproxy: update to 0.2.1
- allow IGMPv3 for LAN
- IGMP proxy: add the possiblity for a custom config (instead of the tomato default)
- change label/description "Efficient Multicast Forwarding" at advanced-routing.asp to "Efficient Multicast Forwarding (IGMP Snooping)"
- add function init() to advanced-firewall.asp (use class attribute for IGMP proxy links to open a new tab/window)
- fix typo at IGMP proxy notes section (wrong example value for downstream threshold) --> default to 1
- update for emf-files and igs-files up to Asus 378_4585
- pptpd: clean sources, add patch instead: change number of default connections to 6, fix for wrong location of binaries
- rp-pppoe: clean sources 3.12, add (forgotten) patch instead
- busybox: enable TEE command
- Revert "QOS: fix the # number of Rule doesn't show in QOS Details view."
- router/Makefile: Added symlink to iptables-save command
- router/Makefile: add "--ipv6" to miniupnpd-config AND fix compilation for ARM bring back IPv6 support
- Revert "router/rc/init.c: R8000: invert the default order of ports"
- router/shared/defaults.c: add missing "ipv6_dhcpd" at router/shared/defaults.c and set it to "1" (Enable DHCPv6)
- router/shared/defaults.c: disable "nf_sip" by default (GUI @ Tracking / NAT Helpers SIP - Option Off)
- www: Modified Bandwidth Limiter warnings
- www.tomato.js: fix typo
- www: about.asp: Cosmetics
- BWL: Manipulate waniface only if QoS is Disabled
- fpkg: remove unused variable
- rc/init.c: improve invalid_mac check
- rc/services.c: remove forgotten reference to stop_zebra()
- root dhcp6c: do not open a routing socket that's never used
- dhcpv6: RENEW: ignore advertise messages with none of requested data and missed status codes
- dhcpv6: small code cleanup
- dhcpv6: ignore advertise messages with none of requested data and missed status codes
- dhcpv6: close file descriptors on exec
- dhcpv6: no need for sizeoff(char)
- dhcpv6: Fix a number of resource/memory leaks
- Fixing use of memset
- Fix dhcp6 parallel build failure with poudriere on FreeBSD, by implementing patch from bug 38: https://sourceforge.net/p/wide-dhcpv6/bugs/38/
- Resolve bind(control sock): Address already in use error Patch #1 from: https://sourceforge.net/p/wide-dhcpv6/bugs/36/
- Resolve bind(control sock): Address already in use issue Patch #2 from https://sourceforge.net/p/wide-dhcpv6/bugs/36/
- IGMP - Resolve CVE-2012-0207 - Resolve potential for divide by 0, allowing remote attackers to cause a denial of service via IGMP packets
- Fix potential FILE * resource leak
- Fix bad memset in auth.c
- Allow for NULL termination on variable partname by increasing its size from 16 to 17
- Rework save_variables procedure so that sprintf is not writing to the same variable, in which case the results are considered undefined
- Fix potential FILE * leak in nvram_commit
- minidlna: patch: add missing if() statement MIA/fix in patch
- IPROUTE - Fix a few resource leaks
- fix some build warnings
- Cleanup tree
- Added Dlink DIR868L and Xiaomi R1D to compilation


2018.2 - 2018.04.17
----------------------------

- fix problem with passing Tagged/UNtagged on same port when using default vlan


2018.1 - 2018.04.14
----------------------------

- php: updated to 7.2.4
- php: 'mysql' option is no longer supported in PHP7, changed to 'mysqli'
- OpenVPN: updated to 2.4.5
- openssl: updated to 1.0.2o
- miniupnpd: updated to 2.0.20180412
- miniupnpd: changed the coding to use an interface name instead of an IP/netmask
- nginx: updated to 1.13.12
- Adminer: updated to 4.6.2
- dnsmasq: update to 2.80test1
http://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=log
- dnscrypt: change update-resolvers script to process v2 resolvers format
- libncurses: updated to 6.1
- nettle: updated to 3.4
- sqlite: updated to 3.23.1
- MiniDLNA: updated to 1.2.1
- New wireless driver for SDK7 (Fixed KRACK vulnerability)
- e2fsprogs: updated to 1.44.1
- nano: updated to 2.9.5
- fixed FTP data connection fails from WAN side when port is not 21
- transmission: updated to 2.93
- ipset: updated to 6.36
- libcurl: updated to 7.59.0
- libcurl: updated CA certificate bundle as of 2018-03-07
- libusb: update to 1.0.22
- usb_modeswitch: updated to 2.52
- libvorbis: updated to 1.3.6
- tor: updated to 0.3.2.10
- dropbear: updated to 2018.76
- xl2tpd: updated to 1.3.11
- pcre: Updated to 8.42
- busybox: changed uname
- router/rc/wan.c: removed "bump wan state file on connect (don't wait watchdog result)"
- router/rc/wan.c: dnsmasq process was receiving a second SIGINT signal. Instead of triggering another DNSSEC time checking, it was killing process
- router/rc/init.c: R7000/R8000: enable Air Time Fairness by default
- router/rc/services.c: fixes issues with httpd
- router/rc/services.c: SIGINT seems to be issued too soon against dnsmasq - wait one second before doing so
- rc/services.c: Connect On Demand could no longer work as designed, due to address 1.1.1.1 becoming a legit recursive DNS server, so a different IP address was chosen for this purpose
- router/Makefile: enabled mini-gmp, saves 4KB
- router/Makefile: disable RAID (mdadm binary)
- Several kernel patches in SDK6 & SDK7
- Changed Tomato versioning
- kernel: updated drivers/net/ modules:
https://bitbucket.org/kille72/tomato-arm-kille72/commits/72befb92d9bf2671de800c2841a583e2c58e9374
https://bitbucket.org/kille72/tomato-arm-kille72/commits/fb421ca0b97e0dedd4e0a2360fd98a1761e80209
- LED: Preliminary support for 2nd 5Ghz LED on R8000
- multiwan: forgotten kernel updates for sdk7
- busybox: add CONFIG_FEATURE_NETSTAT_PRG to configuration, for netstat -p functionality
- GUI: Air Time Fairness support for R7000/R8000
- RT-AC3200: invert the default order of ports
- R8000: invert the default order of ports
- entware: updated installation script
- watchdog: increase curl timeout from 3 to 5 seconds in ckcurl function - on heavy loaded 3G connection it could make false positives
- GUI: fix channel scan function for WiFi
- GUI: fix problem with passing Tagged/UNtagged on same port when using default vlan
- GUI: basic-network.asp: LCP Echo (Interval|Link fail limit) is used also with PPTP, L2TP and PPP3G so let's make it possible to modify
- GUI: add possibility to change default IP (198.51.100.1) where DNS queries send to trigger connect-on-demand
https://bitbucket.org/kille72/tomato-arm-kille72/commits/6d47b63eae4e35f5cbf2375914a2113af61e8d6e
- cstats: fix excess I/O, reduce console spam
https://bitbucket.org/kille72/tomato-arm-kille72/commits/709e23e7f1d6cbb07f125a4227cbe995f2118f88
- libid3tag: fix build/link error on Ubuntu + some additional fixes
- Fixed TOR build on some systems
- Cleanup of unused components from the tree and Makefiles
- www: default theme - original 'usbblue'
- Rebranding to FreshTomato :)
